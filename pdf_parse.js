const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const pdf = require('pdf-parse');
const needle = require('needle');
//БД
const Sequelize = require("sequelize");

module.exports = function (nameBase, user, password, table){
    const sequelize = new Sequelize(nameBase, user, password, {
        logging: false,
        dialect: "postgres",
        define: {
            timestamps: false
        },
        pool: {
            max: 20,
        }  
    });

    const raspisanie = sequelize.define(table, {
        'id': {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
        },
        'Кафедра': {
        type: Sequelize.STRING,
        allowNull: false,
        },
        'Группа': {
        type: Sequelize.STRING,
        allowNull: false,
        },
        'Дата': {
            type: Sequelize.STRING,
            allowNull: false,
        },
        'День': {
            type: Sequelize.STRING,
            allowNull: false,
        },
        'Номер Пары': {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        'Подгруппа': {
            type: Sequelize.STRING,
        },
        'Вид пар': {
            type: Sequelize.STRING,
        },
        'Пара': {
            type: Sequelize.STRING,
        },
        'Препод': {
            type: Sequelize.STRING,
        },
        'Кабинет': {
            type: Sequelize.STRING,
        },
    });

    sequelize.sync();

    //raspisanie.destroy({truncate: true}); //Удаление прошлых записей в таблице

    //параметры для get запросов, чтобы не терялись pdf
    var needle_params =  { 
        open_timeout: 60000,
        read_timeout: 60000,
        compressed: true,
        stream_length: 0
    }

    needle.get('https://www.vyatsu.ru/studentu-1/spravochnaya-informatsiya/raspisanie-zanyatiy-dlya-studentov.html',needle_params, function (error, response) {
        const dom = new JSDOM(response.body);
        const $ = require("jquery")(dom.window);

        let d = "";
        let date = new Date();
        $("a:contains('c ')").each(function(index, value) {
            d = $(this).text().replace(/\n/g, '').replace(/ +/g, ' ').trim();
            if (date.getFullYear()  >= d.slice(8, 12) && date.getFullYear()  <= d.slice(22, 26) &&
                date.getMonth() + 1 >= d.slice(5, 7)  && date.getMonth() + 1 <= d.slice(19, 21) &&
                date.getDate()      >= d.slice(2, 4)  && date.getDate()      <= d.slice(16, 18))
                        return false;
        });

        $("a:contains('" + d + "')").each(function () {
            needle.get("https://www.vyatsu.ru" + $(this).attr('href'), needle_params, (error, response) => {
                if (error)
                    console.log(error)
                else
                parce(response.body);
            });
        });
    });

    function parce(f){    
        pdf(f).then(function(data) {
            //фикс корявого pdf
            let s = data.text.slice(0, data.text.indexOf("Министерство")) + data.text.slice(data.text.indexOf("\"Вятский государственный университет\"")+37);
            
            let kaf = s.slice(s.indexOf('г.') + 3, s.indexOf('РАСПИСАНИЕ') - 1); 
            let grup = s.slice(s.indexOf('Интервал') + 9, s.indexOf('понедельник') - 1);
            
            let days = [
                'понедельник',
                'вторник',
                'среда',
                'четверг',
                'пятница',
                'суббота',
                'воскресенье',
            ];
                    
            let dni = [];
            let date = [];
            let sdvg = 0;
            for (let i = 0; i < 14; i++) {
                dni[i] = [];
                date[i] = s.slice(s.indexOf(days[i % 7], sdvg) + days[i % 7].length, s.indexOf('08:20-09:50', sdvg + 1));
                sdvg = s.indexOf('08:20-09:50', sdvg + 1);
                dni[i][0] = (s.slice(s.indexOf('08:20-09:50', sdvg) + 11, s.indexOf('10:00-11:30', sdvg) - 2));
                dni[i][1] = (s.slice(s.indexOf('10:00-11:30', sdvg) + 11, s.indexOf('11:45-13:15', sdvg) - 2));
                dni[i][2] = (s.slice(s.indexOf('11:45-13:15', sdvg) + 11, s.indexOf('14:00-15:30', sdvg) - 2));
                dni[i][3] = (s.slice(s.indexOf('14:00-15:30', sdvg) + 11, s.indexOf('15:45-17:15', sdvg) - 2));
                dni[i][4] = (s.slice(s.indexOf('15:45-17:15', sdvg) + 11, s.indexOf('17:20-18:50', sdvg) - 2));
                dni[i][5] = (s.slice(s.indexOf('17:20-18:50', sdvg) + 11, s.indexOf('18:55-20:25', sdvg) - 2));
                dni[i][6] = (s.slice(s.indexOf('18:55-20:25', sdvg) + 11, s.indexOf( days[(i + 1) % 7], sdvg) - 2));
            }
            
            let reg = new RegExp(grup + ", ", "g"); //парсим запись по названию группы (получим подгруппы, если есть)
            for (let i = 0; i < 14; i++) {
                for (let j = 0; j < 7; j++) {
                    let s = dni[i][j].replace(/\n/g, '').replace(/ +/g, ' ').trim().split(reg); //массив подгрупп
                    s.forEach(para => {
                        //Если ничего не нашлось ставим ♥, потому что пустую строчку и NULL регулярка не любит, заменить на пробел нельзя
                        let tes = para.match(/\d\d\sподгруппа/);
                        let pdgr = tes ? tes[0] : "♥";

                        tes = para.match(/\d+-\d\d\d[а-я,a-z,_]?[а-я,a-z,_]?[а-я,a-z,_]?/);
                        let kab = tes ? tes[0] : "♥";

                        tes = para.match(/[А-Я][а-я]*\s[А-Я]\.[А-Я]\./);
                        let prep = tes ? tes[0] : "♥";

                        tes = para.match(/Лабораторная работа|Лекция|Практическое занятие|Семинар/);
                        let vid = tes ? tes[0] : "♥";

                        let reg = new RegExp([pdgr, kab, prep, vid].join('|'),'g');
                        para = para.replace(reg, ' ').trim(); //Чистим запись, останется только пара
                
                        raspisanie.create({
                            'Кафедра': kaf,
                            'Группа': grup,
                            'Дата': date[i].trim(),
                            'День': days[i % 7],
                            'Номер Пары': j + 1,
                            'Подгруппа': pdgr == "♥" ? null : pdgr,
                            'Вид пар': vid == "♥" ? null : vid,
                            'Пара': para ? para : null,
                            'Препод': prep == "♥" ? null : prep,
                            'Кабинет': kab == "♥" ? null : kab,
                        });
                    });
                }
            }
        });
    }
}